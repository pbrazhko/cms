<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MainController
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace Main\Controllers;

use CMS_LIB\MVC\Controller\AbstractController;
use CMS_LIB\MVC\View\View;

class MainController extends AbstractController {

    public function IndexAction() {
        $view = new View();
        $view->asset("Var", $view->layoutName);
        $view->setTitle("Test"," -:- ");
        return $view;
    }

}

?>

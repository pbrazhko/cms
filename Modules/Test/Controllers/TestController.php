<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TestController
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace Test\Controllers;

use CMS_LIB\MVC\Controller\AbstractController;
use CMS_LIB\MVC\View\View;
use Test\Model\TestModel;

class TestController extends AbstractController {

    public function IndexAction() {
        if ($this->Id == 3){
            $items = array(
                "0" => array("name" => "Nexus S", "snippet" => "Fast just got faster with Nexus S."),
                "1" => array("name" => "Motorola XOOM™ with Wi-Fi", "snippet" => "The Next, Next Generation tablet."),
                "2" => array("name" => "MOTOROLA XOOM™", "snippet" => "The Next, Next Generation tablet.")
            );
        }
        else{
            $items = array(
                "0" => array("name" => "Nexus S", "snippet" => "Fast just got faster with Nexus S."),
                "1" => array("name" => "Motorola XOOM™ with Wi-Fi", "snippet" => "The Next, Next Generation tablet."),
                "2" => array("name" => "MOTOROLA Razer HD™", "snippet" => "The Next, Next Generation tablet.")
            );
        }
        return new View(array("Items" => $items));
    }

}

?>

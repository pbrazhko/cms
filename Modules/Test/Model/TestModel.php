<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TestModel
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */
namespace Test\Model;

use CMS_LIB\MVC\Model;
use CMS_LIB\MVC\Model\Model;
use CMS_LIB\MVC\Model\Interfaces;

class TestModel extends Model\Model implements Interfaces\InterfaceModule{

    protected $name = null;
    protected $text = null;
    protected $data = null;
    protected $author = null;

    public function getTableName() {
         return 'News';
    }

    public function getAdapter() {
        return 'Mysqli';
    }

    public function prepareVariables($data) {
        $this->name = (isset($data['name'])?$data['name']:null);
        $this->text = (isset($data['text'])?$data['text']:null);
        $this->data = (isset($data['data'])?$data['data']:null);
        $this->author = (isset($data['author'])?$data['author']:null);
    }
}

?>

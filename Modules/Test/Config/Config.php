<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    'Routes' => array(
        'Test' => array(
            'Route' => '/test[/:Action][/:Id]',
            'Constraints' => array(
                'Action' => '[a-zA-Z0-9_-]*',
                'Id' => '[0-9]*',
            ),
            'Controller' => 'Test\Controllers\TestController'
        )
    ),
    'Views' => array(
        'TemplateDir' => __DIR__ . '/../Views'
    )
);
?>

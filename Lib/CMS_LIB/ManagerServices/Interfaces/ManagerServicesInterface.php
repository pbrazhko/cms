<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */
namespace CMS_LIB\ManagerServices\Interfaces;

use CMS_LIB\ManagerServices;

interface ManagerServicesInterface {
    public function getServices(ManagerServices\ManagerServices $managerServices);
}

?>

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ManagerServices
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace CMS_LIB\ManagerServices;

use CMS_LIB\Exceptions\OverrideAllowException as OverrideAllowExc;

class ManagerServices {

    /**
     * Все используемые сервисы
     *
     * @var Array
     */
    private $services = array();

    /**
     *
     * @var Array
     */
    private $factory = array();

    /**
     *
     * @var boolian
     */
    private $allowOverride = false;

    /**
     *
     * @var array
     */
    private $invalidCharacters = array('-' => '', '_' => '', ' ' => '', '\\' => '', '/' => '');

    /**
     *
     * @param \CMS_LIB\ManagerServices\ManagerServiceConfig $Config
     */
    public function __construct(Interfaces\ManagerServicesInterface $Config = null) {
        if ($Config) {
            $Config->getServices($this);
        }
    }

    /**
     *
     * @param string $name
     * @param mixed $value
     * @return \CMS_LIB\ManagerServices\ManagerServices
     * @throws ManagerService\OverrideAllowException
     */
    public function setService($name, $value) {
        $nName = $this->getValidName($name);

        if (isset($this->services[$nName]) && $this->allowOverride != true) {
            throw new OverrideAllowExc(
                    sprintf(
                            "%s: Преопределение сервисов не возможно", __METHOD__)
                    );
        }

        $this->services[$nName] = $value;

        return $this;
    }

    /**
     *
     * @param string $name
     * @return mixed
     */
    public function get($name) {
        $nName = $this->getValidName($name);

        return (isset($this->services[$nName])) ? $this->services[$nName] : false;
    }

    /**
     *
     * @param string $name
     * @param \CMS_LIB\ManagerServices\ManagerServicesFactoryInterface $instance
     * @return \CMS_LIB\ManagerServices\ManagerServices
     * @throws ManagerService\OverrideallowException
     * @throws \CMS_LIB\Exceptions\SystemException
     */
    public function setFactory($name, $instance) {
        $nName = $this->getValidName($name);
        #echo "<pre>";
        #var_dump($nName,array_keys($this->services));
        if (isset($this->services[$nName]) && $this->allowOverride != true) {
            throw new OverrideAllowExc(
                    sprintf(
                            "%s: Преопределение сервисов (%s) не возможно", __METHOD__, $nName)
            );
        }

        if (is_string($instance) || $instance instanceof Interfaces\ManagerServicesFactoryInterface || is_callable($instance)) {
            try {
                $factory = new $instance;
                $service = $factory->createService($this);
            } catch (\Exception $e) {
                throw new \CMS_LIB\Exceptions\SystemException($e->getMessage());
            }
        }

        $this->factory[$nName] = $instance;
        $this->services[$nName] = $service;

        return $this;
    }

    /**
     *
     * @param string $name
     * @return string
     */
    public function getFactory($name) {
        $nName = $this->getValidName($name);

        return $this->factory[$nName];
    }

    /**
     *
     * @return boolian
     */
    public function getAllowOverride() {
        return $this->allowOverride;
    }

    /**
     *
     * @param string $name
     * @return string
     */
    private function getValidName($name) {
        return str_replace(array_keys($this->invalidCharacters), array_values($this->invalidCharacters), $name);
    }

}

?>

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ManagerAdapter
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace CMS_LIB\ManagerAdapter;

use CMS_LIB\Configuration\Configuration;
use CMS_LIB\ManagerAdapter\Drivers;

class ManagerAdapter {

    const Mysqli = 'Mysqli';
    const PDO = 'pdo_mysql';
    const ODBC = 'odbc';

    private $ManagerServices = null;
    protected $factory = array();

    public function __construct(\CMS_LIB\ManagerServices\ManagerServices $managerServices) {
        $this->ManagerServices = $managerServices;
    }

    public function getAdapter($adapterName) {
        if (isset($this->factory[$adapterName])) {
            return $this->factory[$adapterName];
        }

        return $this->factory[$adapterName] = $this->loadAdapter($adapterName);
    }

    private function loadAdapter($adapterName = ManagerAdapter::Mysqli) {

        $adapter = $this->ManagerServices->get('ApplicationConfig')['Adapters'][$adapterName];

        switch ($adapter['Driver']) {
            case ManagerAdapter::Mysqli:
                $driver = new Drivers\Mysqli\Mysqli($adapter);
                break;
            default:
                throw new \Exception("Driver {$adapter['Driver']} is not supported");
        }

        return $driver;
    }

}

?>

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ParamsContainer
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */
namespace CMS_LIB\ManagerAdapter\Drivers;

class ParamsContainer {

    const T_DOUBLE = 'double';
    const T_INTEGER = 'integer';
    const T_STRING = 'string';
    const T_NULL  = 'null';
    const T_LOB = 'lob';

    protected $data = null;

    protected $types = null;

    public function __construct(array $data = array()) {
        if ($data){
            $this->setFromArray($data);
        }
    }

    public function setFormArray($data){
        foreach ($data as $key=>$value){
            $this->setOffset($k, $value);
        }
    }

    public function setOffset($name, $value, $type = null){
        $this->data[$name] = $value;

        $names = array_keys($this->data);
        $key = array_search($name, $names);

        if ($type){
            $this->setType($key, $value);
        }
    }

    public function getOffset($name){
        return (isset($this->data[$name])) ? $this->data[$name] : null ;
    }

    public function getOffsets(){
        return $this->data;
    }

    public function setType($key, $value){
        $this->types[$key] = $value;
    }

    public function getType($name){
        if (is_string($name)){
            $names = array_keys($this->data);
            $name = array_search($name, $names);
        }

        return $this->types[$name];
    }

    public function existsOffset($name){
        return isset($this->data[$name]);
    }

    public function count(){
        return count($this->data);
    }
}

?>

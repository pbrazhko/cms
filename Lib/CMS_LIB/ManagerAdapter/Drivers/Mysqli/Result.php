<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Result
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace CMS_LIB\ManagerAdapter\Drivers\Mysqli;

use CMS_LIB\Exceptions;

class Result {

    protected $resource = null;
    protected $position = 0;
    protected $numberOfRows = -1;
    protected $currentComplete = false;
    protected $nextComplete = false;
    protected $currentData = false;
    protected $statementBindValues = array('keys' => null, 'values' => array());
    protected $generatedValue = null;

    public function initialize($resource, $generatedValue) {
        if (!$resource instanceof \mysqli && !$resource instanceof \mysqli_result && !$resource instanceof \mysqli_stmt) {
            throw new Exception\InvalidArgumentException('Invalid resource provided.');
        }

        $this->resource = $resource;
        $this->generatedValue = $generatedValue;
        return $this;
    }

    public function getResource() {
        return $this->resource;
    }

    public function isQueryResult() {
        return ($this->resource->field_count > 0);
    }

    public function getAffectedRows() {
        if ($this->resource instanceof \mysqli || $this->resource instanceof \mysqli_stmt) {
            return $this->resource->affected_rows;
        } else {
            return $this->resource->num_rows;
        }
    }

    public function current() {
        if ($this->currentComplete) {
            return $this->currentData;
        }

        if ($this->resource instanceof \mysqli_stmt) {
            $this->loadDataFromMysqliStatement();
            return $this->currentData;
        } else {
            $this->loadFromMysqliResult();
            return $this->currentData;
        }
    }

    protected function loadDataFromMysqliStatement() {
        $data = null;
        // build the default reference based bind structure, if it does not already exist
        if ($this->statementBindValues['keys'] === null) {
            $this->statementBindValues['keys'] = array();
            $resultResource = $this->resource->result_metadata();
            foreach ($resultResource->fetch_fields() as $col) {
                $this->statementBindValues['keys'][] = $col->name;
            }
            $this->statementBindValues['values'] = array_fill(0, count($this->statementBindValues['keys']), null);
            $refs = array();
            foreach ($this->statementBindValues['values'] as $i => &$f) {
                $refs[$i] = &$f;
            }
            call_user_func_array(array($this->resource, 'bind_result'), $this->statementBindValues['values']);
        }

        if (($r = $this->resource->fetch()) === null) {
            return false;
        } elseif ($r === false) {
            throw new Exception\RuntimeException($this->resource->error);
        }

        // dereference
        for ($i = 0; $i < count($this->statementBindValues['keys']); $i++) {
            $this->currentData[$this->statementBindValues['keys'][$i]] = $this->statementBindValues['values'][$i];
        }
        $this->currentComplete = true;
        $this->nextComplete = true;
        $this->position++;
        return true;
    }

    /**
     * Load from mysqli result
     *
     * @return boolean
     */
    protected function loadFromMysqliResult() {
        $this->currentData = null;

        if (($data = $this->resource->fetch_assoc()) === null) {
            return false;
        }

        $this->position++;
        $this->currentData = $data;
        $this->currentComplete = true;
        $this->nextComplete = true;
        $this->position++;
        return true;
    }

    /**
     * Next
     *
     * @return void
     */
    public function next() {
        $this->currentComplete = false;

        if ($this->nextComplete == false) {
            $this->position++;
        }

        $this->nextComplete = false;
    }

    /**
     * Key
     *
     * @return mixed
     */
    public function key() {
        return $this->position;
    }

    /**
     * Rewind
     *
     * @throws Exception\RuntimeException
     * @return void
     */
    public function rewind() {
        $this->resource->data_seek(0); // works for both mysqli_result & mysqli_stmt
        $this->currentComplete = false;
        $this->position = 0;
    }

    /**
     * Valid
     *
     * @return boolean
     */
    public function valid() {
        if ($this->currentComplete) {
            return true;
        }

        if ($this->resource instanceof \mysqli_stmt) {
            return $this->loadDataFromMysqliStatement();
        } else {
            return $this->loadFromMysqliResult();
        }
    }

    /**
     * Count
     *
     * @throws Exception\RuntimeException
     * @return integer
     */
    public function count() {
        return $this->resource->num_rows;
    }

    /**
     * Get field count
     *
     * @return integer
     */
    public function getFieldCount() {
        return $this->resource->field_count;
    }

    /**
     * Get generated value
     *
     * @return mixed|null
     */
    public function getGeneratedValue() {
        return $this->generatedValue;
    }

}

?>

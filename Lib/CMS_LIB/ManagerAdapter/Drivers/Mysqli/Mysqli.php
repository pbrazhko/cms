<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mysqli
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace CMS_LIB\ManagerAdapter\Drivers\Mysqli;

class Mysqli extends \mysqli_stmt {

    protected $connection = null;
    protected $declaration = null;
    protected $result = null;

    public function __construct($connection, $declaration = null, $result = null) {
        if (!$connection instanceof Connection) {
            $this->setConnection(new Connection($connection));
        }

        $this->setDeclaration(($declaration) ? : new Declaration());
        $this->setResult(($result) ? : new Result());
    }

    public function setConnection(Connection $connection) {
        $this->connection = $connection;
        $this->connection->setDriver($this);
    }

    public function setDeclaration(Declaration $declasration) {
        $this->declaration = $declasration;
        $this->declaration->setDriver($this);
    }

    public function setResult(Result $result) {
        $this->result = $result;
    }

    public function createDeclaration($sql = null) {
        $declaration = clone $this->declaration;
        if ($sql instanceof mysqli_stmt) {
            $declaration->setResource($sql);
        } else {
            if (is_string($sql)) {
                $declaration->setSql($sql);
            }
            if (!$this->connection->isConnected()) {
                $this->connection->connect();
            }
        }

        $declaration->init($this->connection->getConnection());

        return $declaration;
    }

    public function createResult($resource) {
        $result = clone $this->result;
        $result->initialize($resource, $this->connection->getID());
        return $result;
    }

    public function getConnection() {
        return $this->connection;
    }

}

?>

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Connection
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */
namespace CMS_LIB\ManagerAdapter\Drivers\Mysqli;

use CMS_LIB\Exceptions;

class Connection {

    protected $params = array();

    protected $resource = null;

    protected $driver = null;

    protected $inTransaction = false;

    public function __construct($connection) {
        if (is_array($connection)){
            $this->setParams($connection);
        }
        else if(is_object($connection)){
            $this->setParams((array) $connection);
        }
        else{
            throw new Exceptions\InvalidArgument("\$connection must be an array of parameters");
        }
    }

    public function setParams($connectionParams){
        $this->params = $connectionParams;

        return $this;
    }

    public function getParams(){
        return $this->params;
    }

    public function setDriver(Mysqli $driver){
        $this->driver = $driver;

        return $this;
    }

    public function connect(){
        if ($this->resource instanceof \mysqli){
            return;
        }

        $p = $this->params;

        $findParams = function (array $names) use ($p){
            foreach ($names as $name) {
                if (isset($p[$name])) {
                    return $p[$name];
                }
            }
            return;
        };

        $host   = $findParams(array('Host','host','Hostname','hostname'));
        $user   = $findParams(array('User','user','Username','username'));
        $pass   = $findParams(array('Pass','pass','Password','password'));
        $dbname = $findParams(array('database','Database','dbname', 'db','DB', 'schema'));
        $port   = (isset($p['port'])) ? (int) $p['port'] : null;
        $socket = (isset($p['socket'])) ? $p['socket'] : null;

        $this->resource = new \mysqli();
        $this->resource->init();

        if (!empty($p['driver_options'])) {
            foreach ($p['driver_options'] as $option => $value) {
                if (is_string($option)) {
                    $option = strtoupper($option);
                    if (!defined($option)) {
                        continue;
                    }
                    $option = constant($option);
                }
                $this->resource->options($option, $value);
            }
        }

        $this->resource->real_connect($host, $user, $pass, $dbname, $port, $socket);

        if ($this->resource->connect_error) {
            throw new Exceptions\RuntimeException(
                'Connection error',
                null,
                new Exception\ErrorException($this->resource->connect_error, $this->resource->connect_errno)
            );
        }

        if (!empty($p['charset'])) {
            $this->resource->set_charset($p['charset']);
        }
    }

    public function disconnect(){
        if ($this->resource instanceof \mysqli){
            $this->resource->close();
        }

        unset($this->resource);
    }

    public function isConnected(){
        return ($this->resource instanceof \mysqli);
    }

    public function getConnection(){
        return $this->resource;
    }

    public function commit(){
        if (!$this->resource){
            $this->connect();
        }

        $this->resource->commit();
        $this->inTransaction = false;
    }

    public function rollback(){
        if (!$this->resource) {
            throw new Exceptions\RuntimeException('Must be connected before you can rollback.');
        }

        if (!$this->inTransaction) {
            throw new Exceptions\RuntimeException('Must call commit() before you can rollback.');
        }

        $this->resource->rollback();
        return $this;
    }

    public function execute($sql)
    {
        if (!$this->isConnected()) {
            $this->connect();
        }

        $resultResource = $this->resource->query($sql);

        // if the returnValue is something other than a mysqli_result, bypass wrapping it
        if ($resultResource === false) {
            throw new Exceptions\InvalidException($this->resource->error);
        }

        $resultPrototype = $this->driver->createResult(($resultResource === true) ? $this->resource : $resultResource);
        return $resultPrototype;
    }

    public function getID(){
        return $this->resource->insert_id;
    }
}

?>

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Declaration
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace CMS_LIB\ManagerAdapter\Drivers\Mysqli;

use CMS_LIB\Exceptions;
use CMS_LIB\ManagerAdapter\Drivers\ParamsContainer;

class Declaration {

    protected $mysqli = null;
    protected $driver = null;
    protected $resource = null;
    protected $sql = null;
    protected $isPrepared = false;
    protected $paramsContainer = null;

    public function setDriver(Mysqli $driver) {
        $this->driver = $driver;
        return $this;
    }

    public function isPrepared() {
        return $this->isPrepared;
    }

    public function setResource(\mysqli_stmt $resource) {
        $this->resource = $resource;
        $this->isPrepared = true;
        return $this;
    }

    public function setSql($sql) {
        $this->sql = $sql;
        return $this;
    }

    public function getSql() {
        return $this->sql;
    }

    public function init(\mysqli $mysqli){
        $this->mysqli = $mysqli;
        return $this;
    }

    public function prepare($sql = null) {
        if (null === $sql && $this->isPrepared) {
            throw new Exceptions\RuntimeException("Declaration is already!");
        }

        $this->sql = ($sql) ? : $this->sql;

        $this->resource = $this->mysqli->prepare($this->sql);
        if (!$this->resource instanceof \mysqli_stmt) {
            throw new Exceptions\InvalidQueryException(
                    'Statement couldn\'t be produced with sql: ' . $sql,
                    null,
                    new \ErrorException($this->mysqli->error, $this->mysqli->errno)
            );
        }

        $this->isPrepared = true;
        return $this;
    }

    public function execute($params = null){
        if (!$this->isPrepared){
            $this->prepare();
        }

        if (!$this->paramsContainer instanceof ParamsContainer){
            if ($params instanceof ParamsContainer){
                $this->paramsContainer = $params;
            }
            else{
                $this->paramsContainer = new ParamsContainer();

                if (is_array($params)){
                    $this->paramsContainer->setFormArray($params);
                }
            }
        }

        if ($this->paramsContainer->count() > 0){
            $this->bindParams();
        }

        if ($this->resource->execute() === false) {
            throw new Exception\RuntimeException($this->resource->error);
        }

        $this->resource->store_result();
        $this->isPrepared = false;
        
        $result = $this->driver->createResult($this->resource);
        return $result;
    }

    public function bindParams(){
        $params = $this->paramsContainer->getOffsets();
        $args = array();
        $types = '';

        foreach ($params as $key => $value){
            switch ($this->paramsContainer->getType($key)){
                case ParamsContainer::T_DOUBLE:
                    $types .= 'd';
                    break;
                case ParamsContainer::T_INTEGER:
                    $types .= 'i';
                    break;
                case ParamsContainer::T_NULL:
                    $value = null;
                    break;
                default :
                    $types .= 's';
            }
            $args[] = $value;
        }

        if ($args){
            $this->resource->bind_param($args);
        }
    }
}

?>

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ManagerModules
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace CMS_LIB\ManagerModules;

use CMS_LIB\Exceptions\ModuleException;
use CMS_LIB\AutoLoader\ClassLoader;
use CMS_LIB\Exceptions;
use CMS_LIB\Http;
use CMS_LIB\ManagerServices\ManagerServices;

class ManagerModules {

    /**
     * Все модули прописанные в config
     *
     * @var Array
     */
    public $modules = array();

    /**
     * Инициализированные модули
     *
     * @var type
     */
    public $factory = array();

    /**
     *
     * @var String
     */
    private $url = null;

    /**
     * Объект класса ManagerService
     *
     * @var CMS_LIB\ManagerServices\ManagerServices
     */
    private $ManagerServices = null;

    /**
     * Конструктор
     *
     * @param \CMS_LIB\ManagerServices\ManagerServices $ManagerServices
     */
    public function __construct(ManagerServices $ManagerServices) {
        $this->ManagerServices = $ManagerServices;

        $this->url = $this->ManagerServices->get('Request')->getUrl();
    }

    /**
     * Загружает конфигурации модулей
     *
     * @param type $instConfig
     */
    public function loadModules() {
        $this->modules = array_merge($this->modules, (Array) $this->ManagerServices->get('ApplicationConfig')['Modules']);

        if (count($this->modules) > 0) {
            foreach ($this->modules as $v) {
                $this->ManagerServices->get('Configuration')->addConfig('Modules' . DIRECTORY_SEPARATOR . $v, 'Config/Config.php');
            }
        }
    }

    /**
     * Инициализирует модуль
     *
     * @param type $module
     * @param type $route
     * @return type object
     * @throws InvalidArgument
     */
    public function initModule($module, $route) {
        if (!in_array($module, $this->modules)) {
            throw new Exceptions\InvalidArgument('Не допустимое значение для \$module, модуль не существует!');
        }

        if (isset($this->factory[$module][$route]))
            return $this->factory[$module][$route];

        ClassLoader::getInstance()
                ->add($module, 'Modules');

        $prefix = 'Modules' . DIRECTORY_SEPARATOR . $module;
        $routes = $this->ManagerServices->get('Configuration')->$prefix->Routes;

        if (!ClassLoader::getInstance()->findFile($routes[$route]['Controller'])) {
            throw new ModuleException(sprintf("Контроллер не найден."));
        }

        $this->factory[$module][$route] = new $routes[$route]['Controller'];

        $this->factory[$module][$route]->setManagerServices($this->ManagerServices);

        return $this->factory[$module][$route];
    }

    /**
     * Инициализация и выполнение модуля, для использования в шаблонах
     *
     * @param type $url
     * @return boolean
     * @throws \Exception
     */
    public function executeModule($url = null) {

        if (null !== $url) {
            $this->setUrl($url);
        }

        $route = $this->ManagerServices->get('Route')->getRoute($this->modules, $this->url);

        if ($route === null) {
            return false;
        }

        if (!isset($route->Constraints->Action) || empty($route->Constraints->Action)) {
            $route->Constraints->Action = 'Index';
        }

        $module = $this->initModule($route->Module, $route->Route);
        $result = call_user_func(array($module, $route->Constraints->Action));

        if (!$result instanceof \CMS_LIB\MVC\View\View) {
            throw new \Exception("Результат выполнения модуля '" . $route->Module . "' не соответствует требованиям!");
            return false;
        }

        $result->setCurentRoute($route);

        return $result->display($this->ManagerServices->get('Configuration'));
    }

    /**
     *
     * @param type $name
     * @return type boolian
     */
    public function __isset($name) {
        return isset($this->factory[$name]);
    }

    /**
     *
     * @param type $name
     * @return type object
     */
    public function __get($name) {
        if (isset($this->factory[$name])) {
            return $this->factory[$name];
        }
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function getUrl() {
        return $this->url;
    }

}

?>

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Meta
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */
namespace CMS_LIB\Tags\TagGenerators;

use CMS_LIB\Tags\Interfaces\TagGenerator;

class Meta extends TagAbstract implements TagGenerator{
    public $tagName = 'meta';
    public $isClosed = false;
    
    public function getParametrs() {
        return array(
            'charset' => '',
            'content' => '',
            'http-equiv' => '',
            'name' => '',
        );
    }
}

?>

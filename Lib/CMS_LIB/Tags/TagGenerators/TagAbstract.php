<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TagAbstract
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */
namespace CMS_LIB\Tags\TagGenerators;

class TagAbstract extends \stdClass {
    public $params = '';
    
    public function getTag(Array $arguments){
        
        $this->generateParams($arguments);
        
        $tag = '<' . $this->tagName . $this->params . '>';
        
        if ($this->isClosed){
            $tag .= '</' . $this->tagName . '>';
        }
        
        return $tag;
    }
    
    protected function generateParams(Array $arguments){
        
        $a = $this->getParametrs();
        $searchParams = function ($name) use ($arguments, $a){
            if (isset($arguments[$name])){
                
                if (isset($a[$name]['final']) && $a[$name]['final'] === true && $a[$name]['value']){
                    return $a[$name]['value'];
                }
                
                return $arguments[$name];
            }
            
            return false;
        };
        
        if (is_array($a) && count($a) > 0){
            foreach ($a as $param => $default){
                $valueParam = $searchParams($param);
                if ($valueParam){
                   $this->params .= ' ' . $param . '="' . $valueParam . '"';
                }
            }
        }
    }
}

?>

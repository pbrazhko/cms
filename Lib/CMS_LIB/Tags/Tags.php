<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tags
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */
namespace CMS_LIB\Tags;

use CMS_LIB\Exceptions;

class Tags {
    public $allTags = array(
        'meta' => 'Meta'
    );
    
    private static $_instance = null;
    
    public static function getInstance(){
        if (null === self::$_instance){
            self::$_instance = new self();
        }
        
        return self::$_instance;
    }
    
    private function __construct() {}


    public function generate($nameTag, $arguments){
        if (!isset($this->allTags[$nameTag])){
            throw new Exceptions\InvalidArgument("Недопустимое значение \$nameTag");
        }
        
        $className = __NAMESPACE__ . '\\TagGenerators\\' . $this->allTags[$nameTag];
        
        if (!class_exists($className)){
            throw new Exceptions\SystemException('Класс не найден "'.$classname.'"');
        }
        
        $c = new $className();
        
        return $c->getTag($arguments);
    }
}

?>

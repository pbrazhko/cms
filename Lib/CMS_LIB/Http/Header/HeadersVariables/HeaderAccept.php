<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HeaderAccept
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */
namespace CMS_LIB\Http\Header\HeadersVariables;

class HeaderAccept extends HeadersVariablesAbstract{
    public $variable = 'Accept';

    public function getValue() {
        return explode(",", $this->headerValue);
    }
}

?>

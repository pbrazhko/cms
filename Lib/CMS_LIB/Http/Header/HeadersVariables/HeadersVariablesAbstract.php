<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author root
 */

namespace CMS_LIB\Http\Header\HeadersVariables;

use CMS_LIB\Exceptions;

abstract class HeadersVariablesAbstract {

    var $headerValue = null;

    public function getVariable($headers) {

        if ($this->variable === null)
            throw new Exceptions\InvalidArgument('Не допустимое значение \$headers');

        if (isset($headers[$this->variable])) {
            $this->headerValue = $headers[$this->variable];
        }
    }

    public function getValue(){
        return $this->headerValue;
    }

}

?>

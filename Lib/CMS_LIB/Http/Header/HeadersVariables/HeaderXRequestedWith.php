<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HeaderXRequestedWith
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace CMS_LIB\Http\Header\HeadersVariables;

class HeaderXRequestedWith extends HeadersVariablesAbstract {

    public $variable = 'X-Requested-With';

}

?>

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Headers
 *
 * @author root
 */

namespace CMS_LIB\Http\Header;

use CMS_LIB\Http\Header\HeadersVariables;
use CMS_LIB\Exceptions;

class Headers {

    /**
     * Все хедеры полученные из метода request_headers
     *
     * @var Array
     */
    protected $headers = array();

    /**
     * Обработчики хедеров
     *
     * @var Array
     */
    protected $allHeader = array(
        'X-Requested-With' => 'HeaderXRequestedWith',
        'User-Agent' => 'HeaderUserAgent',
        'Host' => 'HeaderHost',
        'Accept' => 'HeaderAccept'
    );

    /**
     * Конструктор
     */
    public function __construct() {
        $this->headers = $this->request_headers();
    }

    /**
     * Возвращает значение хедера
     *
     * @param String $header
     * @return String
     * @throws Exceptions\InvalidArgument
     */
    public function getHeaders($header) {

        if (!isset($this->allHeader[$header])) {
            throw new Exceptions\InvalidArgument('Не допустимое значение \$header');
        }

        $className = __NAMESPACE__ . '\\HeadersVariables\\' . $this->allHeader[$header];

        if (class_exists($className)) {
            $c = new $className();
            $c->getVariable($this->headers);

            return $c->getValue();
        }
    }

    /**
     *      *
     * @return Array
     */
    private function request_headers() {
        $result = array();

        if (isset($_SERVER['HTTP_HOST']))
            $result['Host'] = $_SERVER['HTTP_HOST'];
        if (isset($_SERVER['HTTP_USER_AGENT']))
            $result['User-Agent'] = $_SERVER['HTTP_USER_AGENT'];
        if (isset($_SERVER['HTTP_ACCEPT']))
            $result['Accept'] = $_SERVER['HTTP_ACCEPT'];
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
            $result['Accept-Language'] = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
        if (isset($_SERVER['HTTP_ACCEPT_ENCODING']))
            $result['Accept-Encoding'] = $_SERVER['HTTP_ACCEPT_ENCODING'];
        if (isset($_SERVER['HTTP_ACCEPT_CHARSET']))
            $result['Accept-Charset'] = $_SERVER['HTTP_ACCEPT_CHARSET'];
        if (isset($_SERVER['HTTP_KEEP_ALIVE']))
            $result['Keep-Alive'] = $_SERVER['HTTP_KEEP_ALIVE'];
        if (isset($_SERVER['HTTP_CONNECTION']))
            $result['Connection'] = $_SERVER['HTTP_CONNECTION'];
        if (isset($_SERVER['HTTP_REFERER']))
            $result['Referer'] = $_SERVER['HTTP_REFERER'];
        if (isset($_SERVER['HTTP_COOKIE']))
            $result['Cookie'] = $_SERVER['HTTP_COOKIE'];
        if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
            $result['If-Modified-Since'] = $_SERVER['HTTP_IF_MODIFIED_SINCE'];
        if (isset($_SERVER['HTTP_IF_NONE_MATCH']))
            $result['If-None-Match'] = $_SERVER['HTTP_IF_NONE_MATCH'];
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']))
            $result['X-Requested-With'] = $_SERVER['HTTP_X_REQUESTED_WITH'];

        return $result;
    }

}

?>

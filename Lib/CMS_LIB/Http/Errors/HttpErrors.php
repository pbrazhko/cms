<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HttpErrors
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace CMS_LIB\Http\Errors;

use CMS_LIB\Exceptions;

class HttpErrors {

    public $http_version = 'HTTP/1.1';
    public $http_error = array(
        '401' => '401 Unauthorized',
        '404' => '404 Not Found'
    );
    private static $_instance = null;

    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function sendError($codeError) {
        if (!isset($this->http_error[$codeError])) {
            throw new Exceptions\InvalidArgument("Не допустимое значение \$codeError");
        }

        header($this->http_version . ' ' . $this->http_error[$codeError]);
        die();
    }

}

?>

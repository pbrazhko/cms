<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicationJson
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */
namespace CMS_LIB\Http\ResponseAccepts;

use CMS_LIB\Exceptions;

class ApplicationJson {
    public function getResponse($data){
        if (!is_array($data)){
            throw new Exceptions\InvalidArgument(sprintf("%s: Не потдерживаемый формат данных!", __METHOD__));
        }

        return json_encode($data);
    }
}

?>

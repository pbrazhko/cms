<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Response
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace CMS_LIB\Http;

use CMS_LIB\Exceptions;

class Response {

    /**
     * Допустимые форматы ответа
     *
     * @var mixed
     */
    public $Accept = null;

    /**
     *
     * @var Array
     */
    public $AcceptTypes = array(
        'application/json' => 'CMS_LIB\Http\ResponseAccepts\ApplicationJson',
        'application/xml' => 'CMS_LIB\Http\ResponseAccepts\ApplicationXml'
    );

    /**
     * Объект класса ManagerService
     *
     * @var CMS_LIB\ManagerServices\ManagerServices
     */
    private $ManagerServices = null;

    /**
     * Конструктор
     *
     * @param \CMS_LIB\ManagerServices\ManagerServices $managerServices
     */
    public function __construct(\CMS_LIB\ManagerServices\ManagerServices $managerServices) {
        $this->ManagerServices = $managerServices;

        $this->Accept = $this->ManagerServices->get('Request')->getAccept();
    }

    /**
     *
     *
     * @param Array $data
     * @throws Exceptions\SystemException
     */
    public function send($data) {
        if ($this->ManagerServices->get('Request')->isXHR()) {

            if (is_array($this->Accept))
                $this->Accept = $this->Accept[0];

            if (!isset($this->AcceptTypes[$this->Accept])) {
                throw new Exceptions\SystemException('Not supported Accept type (' . ($this->Accept) . ')');
            }

            $_accept = new $this->AcceptTypes[$this->Accept];

            echo $_accept->getResponse($data);
        } else {
            include $data;
        }
    }

}

?>

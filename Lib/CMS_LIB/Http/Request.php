<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Request
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace CMS_LIB\Http;

use CMS_LIB\Http\Header;

class Request extends Header\Headers {

    const METHOD_POST = 'POST';
    const METHOD_GET = 'GET';

    /**
     *
     * @var ArrayOfObject
     */
    public $Route = null;

    /**
     * Объект класса ManagerService
     *
     * @var CMS_LIB\ManagerServices\ManagerServices
     */
    private $ManagerServices = null;

    /**
     * Конструктор
     *
     * @param \CMS_LIB\ManagerServices\ManagerServices $managerServices
     */
    public function __construct(\CMS_LIB\ManagerServices\ManagerServices $managerServices) {
        $this->ManagerServices = $managerServices;
    }

    /**
     * Возвращает метод запроса
     *
     * @return String
     */
    public function getMethod() {
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * Возвращает истину если запрос сделан методом POST
     *
     * @return boolean
     */
    public function isPost() {
        if ($this->getMethod() == self::METHOD_POST) {
            return true;
        }
    }

    /**
     * Возвращает истину если запрос сделан через Ajax
     *
     * @return String
     */
    public function isXHR() {
        $xhr = $this->ManagerServices->get('Headers')->getHeaders('X-Requested-With');
        if ($xhr && $xhr == 'XMLHttpRequest') {
            return true;
        }

        return false;
    }

    /**
     * Возвращает значение заголовка Accept
     *
     * @return String
     */
    public function getAccept() {
        return $this->ManagerServices->get('Headers')->getHeaders('Accept');
    }

    /**
     * Получает текущий роутинг
     *
     * @return Array
     */
    public function getCurrentRoute() {
        if (null === $this->Route) {
            $this->Route = $this->ManagerServices->get('Route')->getRoute(
                    $this->ManagerServices->get('ManagerModules')->modules, $this->ManagerServices->get('Request')->getUrl()
            );
        }

        return $this->Route;
    }

    /**
     * Возвращает URL
     *
     * @return string
     */
    public function getUrl() {
        return (empty($_SERVER['REQUEST_URI'])) ? '/' : $_SERVER['REQUEST_URI'];
    }

}

?>

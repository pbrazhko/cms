<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Route
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace CMS_LIB\Http;

use CMS_LIB\Http\Request;
use CMS_LIB\Configuration\Configuration;
use CMS_LIB\Exceptions\InvalidArgument;

class Route {

    /**
     * Хранит роуты модулей
     *
     * @var Array
     */
    private $factory = array();

    /**
     *
     * @var String
     */
    private $curent = null;

    /**
     *
     * @var String
     */
    private $curentRoute = null;

    /**
     *
     * @var Array
     */
    private $curentConstraints = null;

    /**
     *
     * @var Array
     */
    private $_routes = null;

    /**
     * Объект класса ManagerService
     *
     * @var CMS_LIB\ManagerServices\ManagerServices
     */
    private $ManagerServices = null;

    /**
     * Конструктор
     *
     * @param \CMS_LIB\ManagerServices\ManagerServices $managerServices
     */
    public function __construct(\CMS_LIB\ManagerServices\ManagerServices $managerServices) {
        $this->ManagerServices = $managerServices;
    }

    /**
     * Возвращает маршрут текущего модуля
     *
     * @param array $modules
     * @return object
     */
    public function getRoute($modules, $url) {
        $this->_routes = $this->getRoutes($modules);

        foreach ($this->_routes as $moduleName => $moduleValue) {
            $this->curent = $moduleName;

            foreach ($moduleValue as $routeName => $routeValue) {
                $this->curentRoute = $routeName;

                //Создаем структуру массива Constraints для дальнейшего его заполнения
                if (isset($routeValue->Constraints)) {
                    $this->curentConstraints = $routeValue->Constraints;
                }

                //Создаем шаблон регулярного выражения
                $pattern = preg_replace_callback('#\[\/\:([^\[]*)\]#i', array($this, '_callBack'), $routeValue->Route);

                //Проверяем на соответствие
                if (preg_match('#^' . $pattern . '$#i', $url, $matches)) {

                    $this->factory[$moduleName] = new RouteStd(array(), RouteStd::ARRAY_AS_PROPS);

                    $this->factory[$moduleName]->Pattern = $pattern;
                    $this->factory[$moduleName]->Route = $routeValue->Route;
                    $this->factory[$moduleName]->Constraints = (Object) $this->curentConstraints;

                    $this->factory[$moduleName]->Module = $this->curent;
                    $this->factory[$moduleName]->Route = $routeName;

                    $this->factory[$moduleName] = $this->setConstraints($this->factory[$moduleName], $matches);

                    return $this->factory[$moduleName];
                }

                $this->curentRoute = null;
                $this->curentConstraints = null;
            }

            $this->curent = null;
        }
    }

    /**
     * Возвращает роуты всех модулей прописанных в config
     *
     * @param array $modules
     * @return array
     */
    public function getRoutes($modules) {
        $routes = array();

        if (is_array($modules) && count($modules) > 0) {
            foreach ($modules as $v) {
                $this->ManagerServices->get('Configuration')->addConfig('Modules' . DIRECTORY_SEPARATOR . $v, DIRECTORY_SEPARATOR . 'Config' . DIRECTORY_SEPARATOR . 'Config.php');
                $prefix = 'Modules' . DIRECTORY_SEPARATOR . $v;
                $routes[$v] = $this->ManagerServices->get('Configuration')->$prefix->Routes;
            }
        }
        return $routes;
    }

    /**
     * Устанавливает Constraints найденый регулярным выражением
     *
     * @param \CMS_LIB\Http\RouteStd $route
     * @param array $matches
     * @return \CMS_LIB\Http\RouteStd
     * @throws InvalidArgument
     */
    public function setConstraints($route, $matches) {
        if (!$route instanceof RouteStd) {
            throw new InvalidArgument('Не допустимое значение \$route');
        }

        if (count($route->Constraints) > 0) {
            $i = 1;
            foreach ($route->Constraints as $constrainsName => $constrainsValue) {
                $route->Constraints->$constrainsName = $matches[$i];
                $i++;
            }
        }

        return $route;
    }

    /**
     *
     * @param array $matches
     * @return string
     */
    private function _callBack($matches) {
        $curent = $this->curent;
        $curentRoute = $this->curentRoute;

        if (isset($this->_routes[$curent]->$curentRoute->Constraints->$matches[1])) {
            return '\/?(' . $this->_routes[$curent]->$curentRoute->Constraints->$matches[1] . ')?';
        }
    }

}

?>

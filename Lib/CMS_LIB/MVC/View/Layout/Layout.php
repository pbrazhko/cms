<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Layout
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace CMS_LIB\MVC\View\Layout;

use CMS_LIB\Exceptions;
use CMS_LIB\MVC\View\View;
use CMS_LIB\Tags\Tags;

class Layout {

    /**
     *
     * @var string
     */
    public $content = null;

    /**
     *
     * @var Object
     */
    public $ManagerModules = null;

    /**
     * Заголовок страницы
     *
     * @var string
     */
    protected $title = null;

    /**
     * Кодировка страницы
     *
     * @var string
     */
    protected $charset = null;

    /**
     * Установленные мета таги html
     *
     * @var array
     */
    protected $meta = array();

    /**
     * Установленные javascript файлы
     *
     * @var array
     */
    protected $jscript = array();

    /**
     * Установленные стили
     *
     * @var array
     */
    protected $style = array();

    /**
     *
     * @var array
     */
    protected $variables = array();

    /**
     *
     * @var CMS_LIB\ManagerService\ManagerServices
     */
    private $ManagerServices = null;

    /**
     *
     * @var string
     */
    private $Layout = null;

    public function __construct(\CMS_LIB\ManagerServices\ManagerServices $managerServices, $layout) {
        $this->ManagerServices = $managerServices;

        if (!$layout){
            throw new Exceptions\InvalidArgument('Основной шаблон не определен!');
        }

        $this->Layout = $layout;
    }

    /**
     * Устанавливает заголовок страницы
     *
     * @param String $title
     * @param String $separator
     */
    public function setTitle($title, $separator = ' - ') {
        $this->title .= $separator . $title;
    }

    /**
     * Возвращает заголовок страницы
     *
     * @return String
     */
    public function getTitle() {
        return $this->ManagerServices->get('ApplicationConfig')['Sitename'] . $this->title;
    }

    /**
     * Импортирует заголовок установленный в основном мудуле
     *
     * @param \CMS_LIB\MVC\View\View $view
     */
    private function importTitle(View $view) {
        if (null !== $view->getTitle()) {
            $this->title .= $view->getTitle();
        }
    }

    /**
     * Устанавливает кодировку страницы
     *
     * @param String $charset
     */
    public function setCharset($charset) {
        $this->charset = $charset;
    }

    /**
     * Возвращает кодировку страницы
     *
     * @return String
     */
    public function getCharset() {
        return $this->charset;
    }

    /**
     * Импортирует кодировку установленную в основном модуле
     *
     * @param \CMS_LIB\MVC\View\View $view
     */
    private function importCharset(View $view) {
        if (null !== $view->getCharset()) {
            $this->charset = $view->getCharset();
        }
    }

    /**
     * Возвращает meta теги в виде html
     *
     * @return String
     */
    public function getMeta() {
        return implode('\n\r', $this->meta);
    }

    /**
     * Добавляет meta тег
     *
     * @param String $name
     * @param String $content
     * @todo реализовать через гениратор тегов
     */
    public function setMeta(Array $args) {
        $this->meta[] = Tags::getInstance()->generate('meta', $args);
    }

    /**
     * импортирует meta теги становленные в основном модуле
     *
     * @param \CMS_LIB\MVC\View\View $view
     */
    private function importMeta(View $view) {
        if (is_array($view->getMeta()) && count($view->getMeta()) > 0) {
            $this->meta = array_merge($this->meta, $view->getMeta());
        }
    }

    /**
     *
     * @param string $name
     * @param moxed $value
     * @return boolean
     */
    public function asset($name, $value) {
        return $this->$name = $value;
    }

    /**
     *
     * @param \CMS_LIB\MVC\View\View $view
     */
    private function importVariables(View $view) {
        $viewVariables = $view->getAssets();

        if (is_array($viewVariables) && count($viewVariables) > 0) {
            foreach ($viewVariables as $k => $v) {
                $this->variables[$k] = $v;
            }
        }
    }

    /**
     * Инициализирует менеджер модулей
     */
    public function ManagerModules() {
        if (null === $this->ManagerModules) {
            $this->ManagerModules = $this->ManagerServices->get('ManagerModules');
        }

        return $this->ManagerModules;
    }

    /**
     *
     * @param \CMS_LIB\Http\RouteStd $curentRoute
     * @return boolean
     * @throws Exceptions\InvalidArgument
     * @throws Exceptions\ViewException
     */
    public function display($view) {

        if (!$view instanceof View) {
            throw new Exceptions\InvalidArgument("Не допустимое значение \$view");
        }

        $this->importTitle($view);
        $this->importCharset($view);
        $this->importMeta($view);
        $this->importVariables($view);
        
        if ($this->ManagerServices->get("Request")->isXHR()){
            $this->ManagerServices->get('Response')->send($this->variables);
            return;
        }

        $this->content = $view;

        $this->setMeta(array('http-equiv' => 'Content-Type', 'content' => 'text/html; charset=utf-8'));

        $template = getcwd() .
                DIRECTORY_SEPARATOR .
                'Templates' .
                DIRECTORY_SEPARATOR .
                $this->Layout .
                DIRECTORY_SEPARATOR .
                $this->Layout . '.phtml';

        if (!file_exists($template)) {
            throw new Exceptions\ViewException("Layout не найден!");
        }

        include $template;
    }

    /**
     *
     */
    public function Content() {
        $this->content->display($this->ManagerServices->get('Configuration'));
    }
}

?>
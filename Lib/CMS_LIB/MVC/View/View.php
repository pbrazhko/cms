<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of View
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace CMS_LIB\MVC\View;

use CMS_LIB\ManagerModules\ManagerModules;
use CMS_LIB\Configuration\Configuration;
use CMS_LIB\Exceptions;
use CMS_LIB\Http\RouteStd;

class View {

    /**
     *
     * @var string
     */
    public $title = null;

    /**
     * Кодировка страницы
     *
     * @var string
     */
    protected $charset = null;

    /**
     * Установленные мета таги html
     *
     * @var array
     */
    protected $meta = array();

    /**
     * Установленные javascript файлы
     *
     * @var array
     */
    protected $jscript = array();

    /**
     * Установленные стили
     *
     * @var array
     */
    protected $style = array();

    /**
     * Переменные шаблона
     *
     * @var array
     */
    protected $variables = array();

    /**
     * Объект конфигурации текущего модуля
     *
     * @var object
     */
    private $curentRoute = null;

    public function __construct(Array $var = null) {
        if (count($var) > 0) {
            foreach ($var as $k => $v) {
                $this->asset($k, $v);
            }
        }
    }

    /**
     *
     * @return string
     * @throws Exceptions\ViewException
     */
    public function display(Configuration $Configuration) {

        $prefix = 'Modules' . DIRECTORY_SEPARATOR . $this->curentRoute->Module;

        $template_dir = $Configuration->$prefix->Views->TemplateDir;
        $template = $template_dir .
                DIRECTORY_SEPARATOR .
                $this->curentRoute->Module .
                DIRECTORY_SEPARATOR .
                $this->curentRoute->Constraints->Action .
                DIRECTORY_SEPARATOR .
                $this->curentRoute->Constraints->Action . '.phtml';

        if (!file_exists($template)) {
            throw new Exceptions\ViewException("Шаблон не найден!");
        }

        return include $template;
    }

    /**
     *
     * @param string $name
     * @param string $value
     * @return boolean
     */
    public function asset($name, $value) {
        return $this->$name = $value;
    }

    /**
     * Устанавливает заголовок страницы
     *
     * @param String $title
     * @param String $separator
     */
    public function setTitle($title, $separator = ' - ') {
        $this->title .= $separator . $title;
    }

    /**
     * Возвращает заголовок страницы
     *
     * @return String
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Устанавливает кодировку страницы
     *
     * @param String $charset
     */
    public function setCharset($charset) {
        $this->charset = $charset;
    }

    /**
     * Возвращает кодировку страницы
     *
     * @return String
     */
    public function getCharset() {
        return $this->charset;
    }

    /**
     * Возвращает meta теги в виде html
     *
     * @return String
     */
    public function getMeta() {
        return implode('\n\r', $this->meta);
    }

    /**
     * Добавляет meta тег
     *
     * @param String $name
     * @param String $content
     * @todo реализовать через гениратор тегов
     */
    public function setMeta(Array $args) {
        $this->meta[] = Tags::getInstance()->generate('meta', $args);
    }

    /**
     *
     * @return array
     */
    public function getAssets() {
        return $this->variables;
    }

    /**
     *
     * @param \CMS_LIB\Http\RouteStd $route
     * @return boolean
     * @throws Exceptions\InvalidArgument
     */
    public function setCurentRoute($route) {
        if (!$route instanceof RouteStd) {
            throw new Exceptions\InvalidArgument("Не допустимое значение \$route");
            return false;
        }

        $this->curentRoute = $route;
    }

    public function __isset($name) {
        return isset($this->variables[$name]);
    }

    public function __get($name) {
        if (isset($this->variables[$name])) {
            return $this->variables[$name];
        }
    }

    public function __set($name, $value) {
        $this->variables[$name] = $value;
    }

    public function __unset($name) {
        if (isset($this->variables[$name])) {
            unset($this->variables[$name]);
        }
    }

}

?>

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */
namespace CMS_LIB\MVC\Model\Interfaces;

interface InterfaceModule {
    public function getTableName();

    public function getAdapter();

    public function prepareVariables($data);
}

?>

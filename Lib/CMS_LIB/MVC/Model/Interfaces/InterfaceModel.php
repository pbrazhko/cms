<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */
namespace CMS_LIB\MVC\Model\Interfaces;

interface InterfaceModel {
    public function select($where = null);
    public function insert($set);
    public function update($set, $where = null);
    public function delete($where);
}

?>

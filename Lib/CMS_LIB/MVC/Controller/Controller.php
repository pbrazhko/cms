<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Controller
 *
 * @author root
 */

namespace CMS_LIB\MVC\Controller;

use CMS_LIB\ManagerServices\ManagerServices;
use CMS_LIB\Http\Errors\HttpErrors;
use CMS_LIB\Http;
use CMS_LIB\MVC\View\View;

class Controller {

    /**
     * Объект конфигурации текущего модуля
     *
     * @var object
     */
    private $currentRoute = null;

    /**
     * Объект класса ManagerService
     *
     * @var CMS_LIB\ManagerServices\ManagerServices
     */
    private $ManagerServices = null;

    /**
     * Объект текущего класса
     *
     * @var object
     */
    private static $_instance = null;

    /**
     *
     * @param string $config
     */
    public static function init($config) {
        self::getInstance()->prepare($config);
        self::getInstance()->run();
    }

    /**
     *
     * @return object
     */
    public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Ссылка на конфиг
     * @param string $config
     */
    private function prepare($config) {
        $this->ManagerServices = new ManagerServices(new Services\ManagerServicesConfig($config));
    }

    /**
     *
     */
    private function run() {

        $route = $this->ManagerServices->get('Request')->getCurrentRoute();

        if ($route === null) {
            HttpErrors::getInstance()->sendError('404');
        }

        if (!isset($route->Constraints->Action) || empty($route->Constraints->Action)) {
            $route->Constraints->Action = 'Index';
        }

        $instModule = $this->ManagerServices->get('ManagerModules')->initModule(
                $route->Module, $route->Route
        );

        $view = call_user_func(array($instModule, $route->Constraints->Action));

        if (!$view instanceof View) {
            throw new \Exception("Результат не соответствует требованиям!");
            return false;
        }

        $view->setCurentRoute($route);
        $this->ManagerServices->get('Layout')->display($view);
    }

}

?>

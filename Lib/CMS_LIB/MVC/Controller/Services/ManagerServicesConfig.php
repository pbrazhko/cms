<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ManagerServiceConfig
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */
namespace CMS_LIB\MVC\Controller\Services;

use CMS_LIB\ManagerServices\ManagerServices;
use CMS_LIB\ManagerServices\Interfaces;

class ManagerServicesConfig implements Interfaces\ManagerServicesInterface{
    var $services = array(
        'ApplicationConfig' => array()
    );

    var $factory = array(
        'Configuration' => 'CMS_LIB\MVC\Controller\Services\ConfigurationFactory',
        'Request' => 'CMS_LIB\MVC\Controller\Services\RequestFactory',
        'Headers' => 'CMS_LIB\MVC\Controller\Services\HeadersFactory',
        'Response' => 'CMS_LIB\MVC\Controller\Services\ResponseFactory',
        'Layout' => 'CMS_LIB\MVC\Controller\Services\LayoutFactory',
        'Route' => 'CMS_LIB\MVC\Controller\Services\RouteFactory',
        'ManagerModules' => 'CMS_LIB\MVC\Controller\Services\ManagerModulesFactory',
        'ManagerAdapters' => 'CMS_LIB\MVC\Controller\Services\ManagerAdaptersFactory',
    );

    public function __construct(array $configuration = array()) {

        $this->services['ApplicationConfig'] = $configuration;

        if (isset($configuration['factory'])){
            $this->factory = array_merge($this->factory, $configuration['factory']);
        }
    }

    public function getServices(ManagerServices $managerServices) {
        foreach ($this->services as $k=>$v){
            $managerServices->setService($k, $v);
        }

        foreach ($this->factory as $k=>$v){
            $managerServices->setFactory($k, $v);
        }
    }
}

?>

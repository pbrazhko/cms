<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ManagerModulesFactory
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */
namespace CMS_LIB\MVC\Controller\Services;

use CMS_LIB\ManagerServices\Interfaces;
use CMS_LIB\ManagerServices\ManagerServices;
use CMS_LIB\ManagerModules\ManagerModules;

class ManagerModulesFactory implements Interfaces\ManagerServicesFactoryInterface{
    public function createService(ManagerServices $managerServices) {
        $_ManagerModules = new ManagerModules($managerServices);
        $_ManagerModules->loadModules();
        return $_ManagerModules;
    }
}

?>

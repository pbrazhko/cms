<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConfigurationFactory
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */
namespace CMS_LIB\MVC\Controller\Services;

use CMS_LIB\ManagerServices\Interfaces\ManagerServicesFactoryInterface;
use CMS_LIB\Configuration\Configuration;

class ConfigurationFactory implements ManagerServicesFactoryInterface{
    public function createService(\CMS_LIB\ManagerServices\ManagerServices $managerServices) {
        return new Configuration($managerServices);
    }
}

?>

<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AbstractController
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace CMS_LIB\MVC\Controller;

use CMS_LIB\Exceptions;
use CMS_LIB\Http;
use CMS_LIB\Http\Errors;

abstract class AbstractController {

    private $ManagerServices = null;
    private $ManagerModules = null;
    private $ManagerAdapters = null;

    /**
     *
     * @return type
     */
    public function getManagerServices() {
        return $this->ManagerServices;
    }

    public function setManagerServices(\CMS_LIB\ManagerServices\ManagerServices $managerServices) {
        $this->ManagerServices = $managerServices;
        return $this;
    }

    /**
     *
     * @param string $name
     * @param array $arguments
     * @return View
     * @throws Exceptions\InvalidArgument
     */
    public function __call($name, $arguments = array()) {
        if (!method_exists($this, $name . 'Action')) {
            Errors\HttpErrors::getInstance()->sendError('404');
        }

        return call_user_func(array($this, $name . 'Action'), $arguments);
    }

    public function isPost() {
        return Http\Request::getInstance()->isPOST();
    }

    public function __get($name) {
        if(isset($this->ManagerServices->get('Request')->Route->Constraints->$name)){
            return $this->ManagerServices->get('Request')->Route->Constraints->$name;
        }
    }

}

?>

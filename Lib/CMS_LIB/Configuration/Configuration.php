<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Configuration
 *
 * @author root
 */

namespace CMS_LIB\Configuration;

use CMS_LIB\Exceptions;

class Configuration extends ConfigurationFactory {

    /**
     *
     * @param string $prefix
     * @param path $config
     * @return \CMS_LIB\Configuration\Configuration
     * @throws Exceptions\InvalidArgument
     */
    public function addConfig($prefix, $config) {

        $prefix = str_replace('\\', DIRECTORY_SEPARATOR, $prefix);

        if (!isset($this->factory[$prefix])) {
            $config = str_replace('\\', DIRECTORY_SEPARATOR, $config);

            $configPath = $prefix . DIRECTORY_SEPARATOR . $config;

            if (!file_exists($configPath)) {
                throw new Exceptions\InvalidArgument("Фаил конфигурации не найден! (".$prefix.")");
            }

            $this->$prefix = include $configPath;
        }

        return $this;
    }

}

?>

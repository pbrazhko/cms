<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConfigurationStd
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */
namespace CMS_LIB\Configuration;

class ConfigurationStd extends \ArrayObject{
    public function __construct($array) {
        if (is_array($array)){
            foreach ($array as $k=>$v){
                if (is_array($v)){
                    $array[$k] = new ConfigurationStd($v, ConfigurationStd::ARRAY_AS_PROPS);
                }
            }
        }

        parent::__construct($array, ConfigurationStd::ARRAY_AS_PROPS);
    }
}

?>

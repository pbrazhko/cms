<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConfigurationFactory
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace CMS_LIB\Configuration;

class ConfigurationFactory {

    protected $factory = array();

    public function __isset($name) {
        return isset($this->factory[$name]);
    }

    public function __get($name) {
        if (isset($this->factory[$name])) {
            return new ConfigurationStd($this->factory[$name],  ConfigurationStd::ARRAY_AS_PROPS);
        }
    }

    public function __set($name, $value) {
        if (is_array($value) && count($value) > 0) {
            $this->factory[$name] = $value;
        }
    }

    public function __unset($name) {
        if (isset($this->factory[$name])) {
            unset($this->factory[$name]);
        }
    }

}

?>

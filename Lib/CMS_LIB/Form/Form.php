<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Form
 *
 * @author Pavel Brazhko <zibox86@mail.ru>
 */

namespace CMS_LIB\Form;

use CMS_LIB\Exceptions;

class Form {
    protected $factory = array();

    public static $_instance = null;

    public static function getInstance(){
        if (null === self::$_instance){
            self::$_instance = new self();
        }

        return self::$_instance;
    }


    public function get($name){
        if (null === $this->factory[$name]){
            throw new Exceptions\InvalidArgument("Не допустимое значение \$name");
        }
    }
    
}

?>

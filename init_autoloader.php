<?php

include __DIR__.'/Lib/CMS_LIB/AutoLoader/ClassLoader.php';

$loader = \CMS_LIB\AutoLoader\ClassLoader::getInstance();

$loader->add('CMS_LIB', 'Lib');

$loader->register(true);

?>
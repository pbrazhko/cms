<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

chdir(dirname(__DIR__));

// Setup autoloading
require 'init_autoloader.php';

CMS_LIB\MVC\Controller\Controller::init(require 'Config/config.php');

?>